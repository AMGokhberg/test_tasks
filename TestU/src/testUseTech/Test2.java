package testUseTech;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Test2 {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\Artur.Samatov\\IdeaProjects\\TestU\\src\\testUseTech\\testUseTech.txt");
        Scanner reader = new Scanner(file);
        String line = reader.nextLine();
        String[] numStr = line.split("\\,");
        int[] numbers = new int[numStr.length];

        int count = 0;
        // Перевод из массива строк в массив чисел
        for (String number : numStr) {
            numbers[count++] = Integer.parseInt(number);
        }

        // Сортировка по возрастанию
        sortInc(numbers);

        // Сортировка по убыванию
        sortDecr(numbers);
    }

    static void sortInc(int mas[]){
        Arrays.sort(mas);
        System.out.println("Сортировка массива по возрастанию " + Arrays.toString(mas));
    }

    static void sortDecr(int mas[]){
        // переменная для временного хранения элемента массива
        int buf;
        // статус сортировки
        boolean sort = false;
        while(!sort){
            sort = true;
            for(int i=0; i<mas.length-1; i++){
                if(mas[i] < mas[i+1]){
                    sort=false;

                    buf = mas[i];
                    mas[i]=mas[i+1];
                    mas[i+1]=buf;
                }
            }
        }
        System.out.println("Сортировка массива по убыванию " + Arrays.toString(mas));
    }
}
