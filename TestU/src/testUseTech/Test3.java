package testUseTech;

public class Test3 {
    public static void main(String[] args) {

        int bound=-1;
        long result, result2;
        if (bound<0) {
            System.out.println("Вы берете факториал от отрицательного числа");
        }
        else
            {
                result = funFact(bound);
                result2 = funFactorial(bound);
                System.out.println("Факториал " + bound + " равен " + result);
                System.out.println("Рекурсивное решение: Факториал " + bound + " равен " + result2);
            }
    }

    // Без рекурсии
    static long funFact(int bound){
        if (bound >=0) {
            long result = 1;
            for (int i = 1; i <= bound; i++) {
                result *= i;
            }
            return result;
        }
        else
            return -1;
    }

    // С рекурсией
    static long funFactorial(int bound){
        if (bound==0) return 1;
            return (bound* funFactorial(bound-1));
    }
}
